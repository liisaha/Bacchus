﻿using Bacchus.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Bacchus.Controllers
{
    public class BidsController : Controller
    {
        private BacchusContext db = new BacchusContext();

        // GET: Bids
        public ActionResult Index()
        {
            var bids = db.Bids.Include(b => b.Product).Include(u => u.User);
            return View(bids);
        }

        // GET: Bids/Create
        public ActionResult Create(int? productId)
        {
            var bids = new Bid();
            if (bids == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            else
            {
                bids = db.Bids.Where(x => x.Id == productId).FirstOrDefault();
                ViewBag.productId = productId;
                return View(bids);
            }
        }

        // POST: Bids/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int ProductId, double Amount, string FirstName, string LastName)
        {
            Bid bid = new Bid();
            bid.ProductId = ProductId;
            bid.Amount = Amount;
            //bid.UserId = FirstName + LastName /*+ DateTime.Now*/;

            db.Bids.Add(bid);
            db.SaveChanges();
            TempData["SavedMessage"] = "Teie pakkumine on salvestatud";

            return RedirectToAction("Index", "Products");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
