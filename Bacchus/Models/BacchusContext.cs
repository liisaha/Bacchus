﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
    public class BacchusContext : DbContext
    {
    
        public BacchusContext() : base("name=BacchusContext")
        {
        }

        public System.Data.Entity.DbSet<Bacchus.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<Bacchus.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<Bacchus.Models.Bid> Bids { get; set; }
    }
}
