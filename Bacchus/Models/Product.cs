﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string ProductDescription { get; set; }
        [Required]
        public string ProductCategory { get; set; }
        public DateTime? BiddingEndDate { get; set; }

        public ICollection<Bid> Bids { get; set; }
    }
}