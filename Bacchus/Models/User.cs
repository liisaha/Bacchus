﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
    public class User
    {
        public string UserId { get; set; }

        [Display(Name = "Eesnimi")]
        [Required(ErrorMessage = "Eesnimi ei või olla tühi.")]
        public string FirstName { get; set; }

        [Display(Name = "Perekonnanimi")]
        [Required(ErrorMessage = "Perekonnanimi ei või olla tühi.")]
        public string LastName { get; set; }

        [Display(Name = "Kuupäev")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Display(Name = "Kellaaeg")]
        [DataType(DataType.Time)]
        public DateTime Time { get; set; }

        public ICollection<Bid> Bids { get; set; }
    }
}