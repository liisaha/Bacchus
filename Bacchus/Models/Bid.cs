﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
    public class Bid
    {
        public int Id { get; set; }

        [Display(Name = "Pakkumine (EUR)")]
        [Required(ErrorMessage = "Pakkumine ei saa olla tühi.")]
        public double Amount { get; set; }

        public int ProductId { get; set; }
        public Product Product { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        [Display(Name = "Nimi")]
        public string UserIdentity
        {
            get
            {
                if (User != null && User.FirstName != null && User.LastName != null)
                {
                    return String.Format("{0} {1}", User.FirstName, User.LastName);
                }
                else
                {
                    return String.Format("");
                }
            }
        }
    }
}